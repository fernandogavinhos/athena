package com.athena.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.athena.model.Aluno;
import com.athena.model.Professor;

@Controller
public class CadastroController {

	@RequestMapping("/cadastro")
	public String cadastro() {
		System.out.println("Acessando o pagina de cadastro");
		return "cadastro/cadastrar-usuario";
	}

	@RequestMapping("cadastrarProfessor")
	public String cadastrarProfessor(Professor professor) {
		System.out.println("Fazendo cadastro de professor" + professor.toString());

		try {
			// TODO: fazer o cadastro do professor no banco de dados
		} catch (Exception e) {
			e.printStackTrace();
			return "cadastro/cadastro-erro";
		}
		return "cadastro/cadastro-sucesso";
	}

	@RequestMapping("cadastrarAluno")
	public String cadastrarAluno(Aluno aluno) {
		System.out.println("Fazendo cadastro de aluno" + aluno.toString());

		try {
			// TODO: fazer o cadastro do aluno no banco de dados
		} catch (Exception e) {
			e.printStackTrace();
			return "cadastro/cadastro-erro";
		}
		return "cadastro/cadastro-sucesso";
	}

}