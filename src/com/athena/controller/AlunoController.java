package com.athena.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.athena.model.Aluno;
import com.athena.model.IniciacaoCientifica;

@Controller
public class AlunoController {

	@RequestMapping("/minhas-iniciacoes")
	public String listarMinhasIniciacoes() {
		System.out.println("Exibindo listagem das iniciações do aluno");
		// TODO: buscar no banco todas as iniciações do aluno
		List<IniciacaoCientifica> lista = new ArrayList<IniciacaoCientifica>();

		if (lista.isEmpty()) {
			// TODO: settar um atributo para false indicando que nao tem nenhum
			// iniciacao
		}
		return "aluno/iniciacao/lista-minhas-iniciacoes";
	}

	@RequestMapping("/editar-perfil-aluno")
	public String trazerPerfil() {
		System.out.println("Buscando as informações de perfil do aluno logado.");

		try {
			// TODO: traxer as informações do banco
			// TODO: listar na tela as informações
		} catch (Exception e) {
			e.printStackTrace();
			return "aluno/edicao/edicao-perfil-erro";
		}
		return "aluno/edicao/edicao-perfil";
	}

	@RequestMapping("editarPerfilAluno")
	public String editarPerfilAluno(Aluno aluno) {
		System.out.println("Fazendo edicao de perfil do " + aluno.toString());

		try {
			// TODO: fazer o merge dos dados do aluno no banco
		} catch (Exception e) {
			e.printStackTrace();
			return "aluno/edicao/edicao-perfil-erro";
		}
		return "aluno/edicao/edicao-perfil-sucesso";
	}

	@RequestMapping("/listar-iniciacao-aluno")
	public String listarIniciacao() {
		System.out.println("Acessando o pagina de listagem de todas as iniciações científicas para alunos");
		// TODO: buscar no banco todas as iniciações e imprimir na tela
		// TODO: exibir na tela campos de remover, editar (caso professor) e de
		// inscrever (caso aluno)
		return "aluno/iniciacao/ver-todas-iniciacoes";
	}

}