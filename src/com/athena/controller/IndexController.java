package com.athena.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexController {

	@RequestMapping("/olaMundoSpring")
	public String execute() {
		System.out.println("Acessando a home");
		return "ok";
	}

	@RequestMapping("/index")
	public String chamarHome() {
		System.out.println("Acessando o index athena");
		return "index";
	}

	@RequestMapping("/si")
	public String iniciacaoSI() {
		System.out.println("Acessando o pagina de inciacao de SI");
		return "iniciacao/detalhes/si";
	}

	@RequestMapping("/marketing")
	public String iniciacaoMarketing() {
		System.out.println("Acessando o pagina de inciacao de Marketing");
		return "iniciacao/detalhes/marketing";
	}

	@RequestMapping("/lzt")
	public String iniciacaoLZT() {
		System.out.println("Acessando o pagina de inciacao de LZT");
		return "iniciacao/detalhes/lzt";
	}

	@RequestMapping("/logout")
	public String logout() {
		System.out.println("Deslogando do sistema.");
		return "index";
	}

}
