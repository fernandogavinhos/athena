package com.athena.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.athena.model.Usuario;

@Controller
public class LoginController {

	@RequestMapping("/login")
	public String login() {
		System.out.println("Acessando o pagina de login");
		return "login/login";
	}

	@RequestMapping("fazerLogin")
	public String fazerLogin(Usuario usuario) {
		System.out.println("Fazendo Login " + usuario.getEmail() + usuario.getSenha());
		List<Usuario> usuarios = retornaUsuariosBanco();

		boolean usuarioValido = validaUsuario(usuario, usuarios);
		if (usuarioValido) {
			// TODO: verificar se usuario � aluno ou professor
			System.out.println("Usuario v�lido");
			if (usuario.getEmail().contains("chaim")) {
				return "professor/bem-vindo-professor";
			} else {
				return "aluno/bem-vindo-aluno";
			}
		}
		return "login/login-invalido";
	}

	private boolean validaUsuario(Usuario usuario, List<Usuario> usuarios) {
		boolean usuarioValido = false;
		for (Usuario usuarioBanco : usuarios) {
			if (usuarioBanco.getEmail().equals(usuario.getEmail()) && //
					usuarioBanco.getSenha().equals(usuario.getSenha())) {
				usuarioValido = true;
			}
		}
		return usuarioValido;
	}

	private List<Usuario> retornaUsuariosBanco() {
		// TODO: implementar busca no banco
		List<Usuario> usuarios = new ArrayList<Usuario>();
		Usuario usuarioBanco1 = new Usuario();
		usuarioBanco1.setEmail("juliane@usp.br");
		usuarioBanco1.setSenha("1234");
		usuarios.add(usuarioBanco1);
		Usuario usuarioBanco2 = new Usuario();
		usuarioBanco2.setEmail("chaim@usp.br");
		usuarioBanco2.setSenha("1234");
		usuarios.add(usuarioBanco2);
		return usuarios;
	}
}