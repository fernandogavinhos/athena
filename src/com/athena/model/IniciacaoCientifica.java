package com.athena.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class IniciacaoCientifica {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int codIC;

	private String nomeIC;
	private String duracao;
	// TODO: transformar em uma lista de cursos no BD
	private String cursos;
	private String descricao;
	@ManyToOne
	private Professor emailProf;

	public int getCodIC() {
		return codIC;
	}

	public void setCodIC(int codIC) {
		this.codIC = codIC;
	}

	public String getNomeIC() {
		return nomeIC;
	}

	public void setNomeIC(String nomeIC) {
		this.nomeIC = nomeIC;
	}

	public String getDuracao() {
		return duracao;
	}

	public void setDuracao(String duracao) {
		this.duracao = duracao;
	}

	public String getCursos() {
		return cursos;
	}

	public void setCursos(String cursos) {
		this.cursos = cursos;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Professor getEmailProf() {
		return emailProf;
	}

	public void setEmailProf(Professor emailProf) {
		this.emailProf = emailProf;
	}

	@Override
	public String toString() {
		return "IniciacaoCientifica [codIC=" + codIC + ", nomeIC=" + nomeIC + ", duracao=" + duracao + ", cursos="
				+ cursos + ", descricao=" + descricao + ", emailProf=" + emailProf + ", getClass()=" + getClass()
				+ ", hashCode()=" + hashCode() + ", toString()=" + super.toString() + "]";
	}

}