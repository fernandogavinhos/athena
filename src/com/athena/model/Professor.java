package com.athena.model;

import javax.persistence.Entity;

@Entity
public class Professor extends Usuario {

	private String cursoMinistra;
	private String linhaPesquisa;

	
	//TODO: icnluir as valida��es com anota��es
	public String getCursoMinistra() {
		return cursoMinistra;
	}

	public void setCursoMinistra(String cursoMinistra) {
		this.cursoMinistra = cursoMinistra;
	}

	public String getLinhaPesquisa() {
		return linhaPesquisa;
	}

	public void setLinhaPesquisa(String linhaPesquisa) {
		this.linhaPesquisa = linhaPesquisa;
	}

	@Override
	public String toString() {
		return "Professor [cursoMinistra=" + cursoMinistra + ", linhaPesquisa=" + linhaPesquisa
				+ ", getCursoMinistra()=" + getCursoMinistra() + ", getLinhaPesquisa()=" + getLinhaPesquisa()
				+ ", getnUsp()=" + getnUsp() + ", getNome()=" + getNome() + ", getEmail()=" + getEmail()
				+ ", getTelefone()=" + getTelefone() + ", getCelular()=" + getCelular() + ", getCurriculoLattes()="
				+ getCurriculoLattes() + ", getSenha()=" + getSenha() + ", getSexo()=" + getSexo() + ", getPapel()="
				+ getPapel() + ", toString()=" + super.toString() + ", getClass()=" + getClass() + ", hashCode()="
				+ hashCode() + "]";
	}

}
