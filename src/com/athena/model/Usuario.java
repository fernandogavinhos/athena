package com.athena.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class Usuario {

	@Id
	private int nUsp;

	private String nome;

	private String email;

	private String telefone;

	private String celular;

	private String curriculoLattes;

	private String senha;

	// TODO: transformar em um enum
	private String sexo;

	// TODO: transformar em um enum
	private String papel;

	// TODO: icnluir as valida��es com anota��es
	public int getnUsp() {
		return nUsp;
	}

	public void setnUsp(int nUsp) {
		this.nUsp = nUsp;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getCurriculoLattes() {
		return curriculoLattes;
	}

	public void setCurriculoLattes(String curriculoLattes) {
		this.curriculoLattes = curriculoLattes;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getPapel() {
		return papel;
	}

	public void setPapel(String papel) {
		this.papel = papel;
	}

	@Override
	public String toString() {
		return "Usuario [nUsp=" + nUsp + ", nome=" + nome + ", email=" + email + ", telefone=" + telefone + ", celular="
				+ celular + ", curriculoLattes=" + curriculoLattes + ", senha=" + senha + ", sexo=" + sexo + ", papel="
				+ papel + "]";
	}

}
