package com.athena.facade;

import com.athena.model.Usuario;

public interface Login {

	public Usuario fazerLogin(String login, String senha);

}
