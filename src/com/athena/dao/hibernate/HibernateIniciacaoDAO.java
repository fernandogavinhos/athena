package com.athena.dao.hibernate;

import java.util.List;

import javax.persistence.EntityManager;

import com.athena.dao.GenericDAO;
import com.athena.model.Curso;
import com.athena.model.IniciacaoCientifica;
import com.athena.model.Usuario;

@SuppressWarnings("unchecked")
public class HibernateIniciacaoDAO extends GenericDAO<Long, IniciacaoCientifica> {

	private EntityManager entityManager;

	public HibernateIniciacaoDAO(EntityManager entityManager) {
		super(entityManager);
		this.entityManager = entityManager;
	}

	public IniciacaoCientifica buscarIniciacaoPorNome(String nome) {
		// TODO: montar a query certa para esta busca
		List<IniciacaoCientifica> lista = entityManager.createQuery("").getResultList();

		return lista.get(0);
	}

	public IniciacaoCientifica buscarIniciacaoPorCodigo(int codIC) {
		// TODO: montar a query certa para esta busca
		List<IniciacaoCientifica> lista = entityManager.createQuery("").getResultList();

		return lista.get(0);
	}

	public IniciacaoCientifica buscarIniciacaoPorCursos(List<Curso> cursos) {
		// TODO: montar a query certa para esta busca
		List<IniciacaoCientifica> lista = entityManager.createQuery("").getResultList();

		return lista.get(0);
	}

	public IniciacaoCientifica buscarIniciacaoPorUsuario(Usuario usuario) {
		// TODO: montar a query certa para esta busca
		List<IniciacaoCientifica> lista = entityManager.createQuery("").getResultList();

		return lista.get(0);
	}
}
