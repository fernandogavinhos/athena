package com.athena.dao.hibernate;

import java.util.List;

import javax.persistence.EntityManager;

import com.athena.dao.GenericDAO;
import com.athena.model.TipoUsuario;
import com.athena.model.Usuario;

@SuppressWarnings("unchecked")
public class HibernateUsuarioDAO extends GenericDAO<Long, Usuario> {

	private EntityManager entityManager;

	public HibernateUsuarioDAO(EntityManager entityManager) {
		super(entityManager);
		this.entityManager = entityManager;
	}

	public Usuario buscarUsuarioPorEmail(String email) {
		// TODO: montar a query certa para esta busca
		List<Usuario> lista = entityManager.createQuery("").getResultList();

		return lista.get(0);
	}

	public Usuario buscarUsuarioPorNome(String nome) {
		// TODO: montar a query certa para esta busca
		List<Usuario> lista = entityManager.createQuery("").getResultList();

		return lista.get(0);
	}

	public Usuario buscarUsuarioPorNUSP(int NUsp) {
		// TODO: montar a query certa para esta busca
		List<Usuario> lista = entityManager.createQuery("").getResultList();

		return lista.get(0);
	}

	public Usuario buscarUsuarioPorTipo(TipoUsuario tipoUsuario) {
		// TODO: montar a query certa para esta busca
		List<Usuario> lista = entityManager.createQuery("").getResultList();

		return lista.get(0);
	}

}
