package com.athena.services;

import java.util.List;

import com.athena.dao.SimpleEntityManager;
import com.athena.dao.hibernate.HibernateUsuarioDAO;
import com.athena.model.TipoUsuario;
import com.athena.model.Usuario;

public class UsuarioServiceImp implements UsuarioService {

	private HibernateUsuarioDAO usuarioDao;

	private SimpleEntityManager simpleEntityManager;

	public UsuarioServiceImp(SimpleEntityManager simpleEntityManager) {
		this.simpleEntityManager = simpleEntityManager;
		usuarioDao = new HibernateUsuarioDAO(simpleEntityManager.getEntityManager());
	}

	@Override
	public boolean cadastrarUsuario(Usuario usuario) {
		try {
			simpleEntityManager.beginTransaction();
			usuarioDao.save(usuario);
			simpleEntityManager.commit();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			simpleEntityManager.rollBack();
			return false;
		}
	}

	@Override
	public boolean editarUsuario(Usuario usuario) {
		try {
			simpleEntityManager.beginTransaction();
			usuarioDao.update(usuario);
			simpleEntityManager.commit();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			simpleEntityManager.rollBack();
			return false;
		}
	}

	@Override
	public boolean removerUsuario(Usuario usuario) {
		try {
			simpleEntityManager.beginTransaction();
			usuarioDao.delete(usuario);
			simpleEntityManager.commit();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			simpleEntityManager.rollBack();
			return false;
		}
	}

	@Override
	public Usuario buscarUsuarioPorEmail(String email) {
		try {
			simpleEntityManager.beginTransaction();
			return usuarioDao.buscarUsuarioPorEmail(email);
		} catch (Exception e) {
			e.printStackTrace();
			simpleEntityManager.rollBack();
			return null;
		}
	}

	@Override
	public Usuario buscarUsuarioPorNome(String nome) {
		try {
			simpleEntityManager.beginTransaction();
			return usuarioDao.buscarUsuarioPorNome(nome);
		} catch (Exception e) {
			e.printStackTrace();
			simpleEntityManager.rollBack();
			return null;
		}
	}

	@Override
	public Usuario buscarUsuarioPorNUSP(int NUsp) {
		try {
			simpleEntityManager.beginTransaction();
			return usuarioDao.buscarUsuarioPorNUSP(NUsp);
		} catch (Exception e) {
			e.printStackTrace();
			simpleEntityManager.rollBack();
			return null;
		}
	}

	@Override
	public Usuario buscarUsuarioPorTipo(TipoUsuario tipoUsuario) {
		try {
			simpleEntityManager.beginTransaction();
			return usuarioDao.buscarUsuarioPorTipo(tipoUsuario);
		} catch (Exception e) {
			e.printStackTrace();
			simpleEntityManager.rollBack();
			return null;
		}
	}

	@Override
	public List<Usuario> buscarTodosUsuarios() {
		return usuarioDao.findAll();
	}

}
