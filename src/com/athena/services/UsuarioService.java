package com.athena.services;

import java.util.List;

import com.athena.model.TipoUsuario;
import com.athena.model.Usuario;

public interface UsuarioService {

	public boolean cadastrarUsuario(Usuario usuario);

	public boolean editarUsuario(Usuario usuario);

	public boolean removerUsuario(Usuario usuario);

	public Usuario buscarUsuarioPorEmail(String email);

	public Usuario buscarUsuarioPorNome(String nome);

	public Usuario buscarUsuarioPorNUSP(int NUsp);

	public Usuario buscarUsuarioPorTipo(TipoUsuario tipoUsuario);

	public List<Usuario> buscarTodosUsuarios();

}
