package com.athena.services;

import java.util.List;

import com.athena.dao.SimpleEntityManager;
import com.athena.dao.hibernate.HibernateIniciacaoDAO;
import com.athena.model.Curso;
import com.athena.model.IniciacaoCientifica;
import com.athena.model.Usuario;

public class IniciacaoServiceImp implements IniciacaoService {

	private HibernateIniciacaoDAO iniciacaoDAO;

	private SimpleEntityManager simpleEntityManager;

	public IniciacaoServiceImp(SimpleEntityManager simpleEntityManager) {
		this.simpleEntityManager = simpleEntityManager;
		iniciacaoDAO = new HibernateIniciacaoDAO(simpleEntityManager.getEntityManager());
	}

	@Override
	public boolean cadastrarIniciacao(IniciacaoCientifica iniciacaoCientifica) {
		try {
			simpleEntityManager.beginTransaction();
			iniciacaoDAO.save(iniciacaoCientifica);
			simpleEntityManager.commit();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			simpleEntityManager.rollBack();
			return false;
		}
	}

	@Override
	public boolean editarIniciacao(IniciacaoCientifica iniciacaoCientifica) {
		try {
			simpleEntityManager.beginTransaction();
			iniciacaoDAO.update(iniciacaoCientifica);
			simpleEntityManager.commit();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			simpleEntityManager.rollBack();
			return false;
		}
	}

	@Override
	public boolean removerIniciacao(IniciacaoCientifica iniciacaoCientifica) {
		try {
			simpleEntityManager.beginTransaction();
			iniciacaoDAO.delete(iniciacaoCientifica);
			simpleEntityManager.commit();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			simpleEntityManager.rollBack();
			return false;
		}
	}

	@Override
	public IniciacaoCientifica buscarIniciacaoPorNome(String nome) {
		try {
			simpleEntityManager.beginTransaction();
			return iniciacaoDAO.buscarIniciacaoPorNome(nome);
		} catch (Exception e) {
			e.printStackTrace();
			simpleEntityManager.rollBack();
			return null;
		}
	}

	@Override
	public IniciacaoCientifica buscarIniciacaoPorCodigo(int codIC) {
		try {
			simpleEntityManager.beginTransaction();
			return iniciacaoDAO.buscarIniciacaoPorCodigo(codIC);
		} catch (Exception e) {
			e.printStackTrace();
			simpleEntityManager.rollBack();
			return null;
		}
	}

	@Override
	public IniciacaoCientifica buscarIniciacaoPorCursos(List<Curso> cursos) {
		try {
			simpleEntityManager.beginTransaction();
			return iniciacaoDAO.buscarIniciacaoPorCursos(cursos);
		} catch (Exception e) {
			e.printStackTrace();
			simpleEntityManager.rollBack();
			return null;
		}
	}

	@Override
	public IniciacaoCientifica buscarIniciacaoPorUsuario(Usuario usuario) {
		try {
			simpleEntityManager.beginTransaction();
			return iniciacaoDAO.buscarIniciacaoPorUsuario(usuario);
		} catch (Exception e) {
			e.printStackTrace();
			simpleEntityManager.rollBack();
			return null;
		}
	}

	@Override
	public List<IniciacaoCientifica> buscarTodasIniciacoes() {
		return iniciacaoDAO.findAll();
	}

}
