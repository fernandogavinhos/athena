<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>

<body>

	<c:import url="/WEB-INF/pages/index/cabecalho.jsp" />

	<!-- projects -->
	<div class="projects">
		<div class="container">
			<div class="projects-info">
				<br> <br> <br>
				<h3>Ocorreu um erro! Sentimos muito! =/</h3>
				<br> <br>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>

	<c:import url="/WEB-INF/pages/index/rodape.jsp" />

</body>

</html>